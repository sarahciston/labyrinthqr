#!/usr/bin/env python

import cv2
import mediapipe as mp
import time

cap = cv2.VideoCapture(0)
mpHands = mp.solutions.hands
hands = mpHands.Hands()
mpDraw = mp.solutions.drawing_utils

handData = []

while True:
    
    frame_time = time.perf_counter_ns() // 1000000 
    
    success, image = cap.read()
    imageRGB = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    results = hands.process(imageRGB)

    if results.multi_hand_landmarks:
        for hand, handLMs in enumerate(results.multi_hand_landmarks):
            for id, lm in enumerate(handLMs.landmark):
                h, w, c = image.shape
                cx, cy = int(lm.x * w), int(lm.y * h)

                if id == 4 or id == 8:
                    cv2.circle(image, (cx, cy), 10, (0,255,0), cv2.FILLED)

                dataset = {
                    "time": frame_time,
                    "hand": hand, 
                    "point": id,
                    "x": lm.x,
                    "y": lm.y,
                    "z": lm.z
                }

                handData.append(dataset)
                # print(id, lm)
    
            mpDraw.draw_landmarks(image, handLMs, mpHands.HAND_CONNECTIONS)

    cv2.imshow("Output", image)
    # print('frame rate: ', frame_rate, 'frame count: ', frame_count)
    cv2.waitKey(1)
    
    # print(handData)
    with open("handData.json", "w") as f:
        f.write(str(handData))

# TK: principle component analysis on primary axis of data output