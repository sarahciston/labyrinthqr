let detector
let objects = []
let video

let SIZE = 600

// smoothing w frame lerp 
let lastX = []
let lastY = []
let lastW = []
let lastH = []
let LERP_PARAM = 0.75

let modelLoaded = () => console.log('model loaded', 'ml5 version:', ml5.version)

function preload(){
    detector = ml5.objectDetector('cocossd', {}, modelLoaded)
    // detector = ml5.objectDetector('mobilenet', {}, modelLoaded)
}

function detected(err, res){
    if(err){ console.log(err)}

    objects = res
    detector.detect(video, detected)
}

function setup(){
    createCanvas(SIZE, SIZE)
    frameRate(10)

    video = createCapture(VIDEO)
    video.size(SIZE, SIZE)
    video.hide()
    detector.detect(video, detected)
}

function bounding(objects){

    for (i in objects){
        // lerpX[i] = 0
        // lerpY[i] = 0

        let object = objects[i]
        stroke(0,255,175)
        strokeWeight(1)
        noFill()

        rect(lerp(lastX, object.x, LERP_PARAM), lerp(lastY, object.y, LERP_PARAM), lerp(lastW, object.width, LERP_PARAM), lerp(lastH, object.height, LERP_PARAM))
        noStroke()
        fill(255)
        textSize(18)
        text(object.label, object.x + 10, object.y - 10)
        
        lastX = object.x
        lastY = object.y
        lastW = object.width
        lastW = object.height
    }
}


function draw(){
    // background(255)
    image(video,0,0)
    bounding(objects)
}