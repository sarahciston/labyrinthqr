#!/usr/bin/env python

import pyglet
from pyglet import shapes, math, clock
# from pyglet.window import key, mouse
# from pyglet.gl import *
from random import randint
import time

import numpy as np

import io
import qrcode

COLOR = (0, 25, 125)

# generate QR code array
qr = qrcode.QRCode(
        version=1,
        box_size=1,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        border=4
    )
qr.add_data('no knots only loops')
qr.make(fit=True)
f = io.StringIO()
qr.print_ascii(out=f)
f.seek(0)
print(f.read())
qrArray = qr.get_matrix()

# create canvas
window = pyglet.window.Window(777, 777, resizable=True)
# 1280x800

# container for things to draw together
batch = pyglet.graphics.Batch()
batchList = []

# initialize grid array w zero content
ROWS = len(qrArray[0])
COLS = len(qrArray)
print('rows: ', ROWS, 'cols: ', COLS)
SIZE = 20
# ROWS = 25
# COLS = 25
# grid = np.zeros((ROWS, COLS), dtype='int16')
# print(grid.flatten())
fullSet = []
walls = []
openSet = []
closedSet = []
stack = []
# start = [0]
# end = [0]
centerLeft = int(ROWS/2)

# def timer():
#     timerDuration = 30
#     while timerDuration > 0:
#         time.sleep(1)
#         timerDuration -= 1


# standard grid index function per DS
def index(c, r):
    if (c < 0 or r < 0 or c > COLS-1 or r > ROWS-1):
        return -1
    return c + r * COLS

class Cell():
    # def __init__(self, r, c): #this needs to change to know where it is in the array
    def __init__(self, value, r, c): #this needs to change to know where it is in the array
        self.value = value #zero or one value in array
        self.r = r #which row position
        self.c = c #which column position
        self.walls = [True, True, True, True] #clockwise: top right bottom left
        self.visited = False
        self.neighbors = []
        # self.index = fullSet.index(self)

    # def getIndex(self):
    #     print(fullSet.index(self))

    def show(self): 
        # initialize drawing
        if (self.value == True):
            fill = (255, 255, 255)
        elif (self.visited == True):
            fill = COLOR
        else:
            fill = (0, 0, 0)
            
        # if (self.visited == True):
        #     lineColor = (0, 255, 0)
        # else: 
        #     lineColor = (0, 0, 0)

        x = self.r * SIZE
        y = self.c * SIZE

        batchList.append(shapes.BorderedRectangle(x, y, SIZE, SIZE, border=0, color=fill, border_color=fill, batch=batch))

        # if (self.walls[0]):
        #     batchList.append(shapes.Line(x, y+1, x + SIZE, y+1, color=lineColor, batch=batch))
        # if (self.walls[1]):
        #     batchList.append(shapes.Line(x + SIZE-1, y, x + SIZE-1, y + SIZE, color=lineColor, batch=batch))
        # if (self.walls[2]):
        #     batchList.append(shapes.Line(x, y + SIZE, x + SIZE, y + SIZE, color=lineColor, batch=batch))
        # if (self.walls[3]):
        #     batchList.append(shapes.Line(x, y, x, y + SIZE, color=lineColor, batch=batch))
        
    def checkNeighbors(self):
        self.neighbors = []
        self.top = fullSet[index(self.c, self.r - 1)]
        self.right = fullSet[index(self.c + 1, self.r)]
        self.bottom = fullSet[index(self.c, self.r + 1)]
        self.left = fullSet[index(self.c - 1, self.r)]

        # add neighbor cell to neigbhor list if exists and not visited
        if (self.top.c < (ROWS - 1) and self.top.value == False):
            self.neighbors.append(self.top)
        if (self.right.r > 0 and self.right.value == False):
            self.neighbors.append(self.right)
        if (self.bottom.c > 0 and self.bottom.value == False):
            self.neighbors.append(self.bottom)
        if (self.left.r < (COLS - 1) and self.left.value == False):
            self.neighbors.append(self.left)

        # select random neighbor
        if (len(self.neighbors) > 0):
            wild = randint(0, len(self.neighbors) - 1)
            # print("chose: ", wild)
            # self.neighbors[wild].visited = True
            return self.neighbors[wild]
        elif (len(stack) > 0):
            current = stack.pop()
        # else: 
        #     stack.append(
        #     print("neighbors broken")


def makeCells():
    global current, next, start, finish, openLength

    # for every row and column in grid array create a Cell class instance
    for iter, r in enumerate(qrArray):
    # for iter, r in enumerate(grid):
        for jter, c in enumerate(r):
            cell = Cell(c, iter, jter)
            fullSet.append(cell)
            if (cell.value == True):
                openSet.append(cell)
            elif (cell.value == False):
                walls.append(cell)
            # print(cell.value, cell.r, cell.c)

    arrayLength = len(fullSet)
    openLength = len(openSet)
    wallsLength = len(walls)
    print('arrayLength is: ', arrayLength, 'openLength is ', openLength, 'wallsLength is ', wallsLength)

    # set the starting and ending points
    centerLeft = int(ROWS/2)
    center = int(len(fullSet)/2)
    bottomCenter = int(center - centerLeft)
    current = fullSet[bottomCenter]
    finish = fullSet[center]

                
def removeWalls(a, b):
    x = a.c - b.c
    if (x == 1): 
        a.walls[3] = False
        b.walls[1] = False
    elif (x == -1):
        a.walls[1] = False
        b.walls[3] = False

    y = a.r - b.r
    if (y == 1): 
        a.walls[0] = False
        b.walls[2] = False
    elif (y == -1):
        a.walls[2] = False
        b.walls[0] = False

def update(dt):
    global current, next
    # print("current cell: ", current.r, current.c, current.value)
    # closedSet.append(current)
    # print(len(closedSet))
    current.visited = True
    
    for g in fullSet:
        g.show()

    # current.neighbors = []
    next = current.checkNeighbors()
    if (next): 
        # removeWalls(current, next)
        next.visited = True
        stack.append(current)
        current = next
        # print("sent to stack")
    # elif (len(stack) > 0):
    #     current = stack.pop()
        # print(len(stack))
        # print("hit stack")

    # timeStart = time.time()
    
    # timeout = 3

    # while True:
    #     timeElapsed = timeStart - time.time()
    #     print(timeElapsed)

    #     if timeElapsed >= timeout:
    #         print('hit timer')
    #         break
    #     if openLength == 0:
    #         break
    #     if current == finish:
    #         break
            
@window.event
def on_draw(): 
    window.clear()    
    batch.draw()
    # label.draw()

@window.event
def on_resize(width, height):
    print('The window was resized to %dx%d' % (width, height))


def main(): 
    while True:

        window.set_minimum_size(400,400)
        window.set_maximum_size(1024,1024)
        # event_logger = pyglet.window.event.WindowEventLogger()
        # window.push_handlers(event_logger)

        makeCells()

        clock.schedule_interval(update, 1/3)
        pyglet.app.run()

if __name__ == '__main__':
    main()






# @window.event
# def on_key_press(symbol, modifiers):
#     if symbol == key.LEFT:
#         print('The left arrow key was pressed.')
#     elif symbol == key.ENTER:
#         print('The enter key was pressed.')
#     else:
#         print('A DIFFERENT key was pressed')

# @window.event
# def on_mouse_press(x, y, button, modifiers):
#     if button == mouse.LEFT:
#         print('The left mouse button was pressed.')

# label = pyglet.text.Label('hello world',
#                             font_name='Avenir',
#                             font_size=36,
#                             x=window.width/2,
#                             y=window.height/2,
#                             anchor_x='center',
#                             anchor_y='center')

