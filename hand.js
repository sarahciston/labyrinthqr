let predictions = []
let video

let SIZE = 900

// smoothing w frame lerp 
let lastX = []
let lastY = []
let lastW = []
let lastH = []
let LERP_PARAM = 0.75

let modelLoaded = () => console.log('model loaded', 'ml5 version:', ml5.version)

// function preload(){
// }

function setup(){
    createCanvas(SIZE, SIZE)
    frameRate(30)

    video = createCapture(VIDEO)
    // video.size(SIZE, SIZE)
    video.hide()
    // detector.detect(video, detected)
    var options = {
        flipHorizontal: false,
        maxContinuousChecks: Infinity,
        detectionConfidence: 0.8,
        scoreThreshold: 0.75,
        iouThreshold: 0.3
    }

    handpose = ml5.handpose(video, options, modelLoaded)
    // detector = ml5.objectDetector('mobilenet', {}, modelLoaded)

    handpose.on('hand', res => {
        predictions = res
        // console.log(res)
    })

    // handpose.predict(video, res => {console.log(res)})
}

// function bounding(predictions){

//     for (i in predictions){
//         // lerpX[i] = 0
//         // lerpY[i] = 0

//         let object = predictions[i]
//         stroke(0,255,175)
//         strokeWeight(1)
//         noFill()

//         rect(lerp(lastX, object.x, LERP_PARAM), lerp(lastY, object.y, LERP_PARAM), lerp(lastW, object.width, LERP_PARAM), lerp(lastH, object.height, LERP_PARAM))
//         noStroke()
//         fill(255)
//         textSize(18)
//         text(object.label, object.x + 10, object.y - 10)
        
//         lastX = object.x
//         lastY = object.y
//         lastW = object.width
//         lastW = object.height
//     }
// }

function drawPoints(){

    for (i in predictions){
        let handJob = predictions[i]
        let hand = handJob.annotations
        console.log(hand)

        stroke(0,125,175)
        // thumb and finger lines
        line(hand.thumb[0][0], hand.thumb[0][1], hand.thumb[1][0], hand.thumb[1][1])
        line(hand.thumb[1][0], hand.thumb[1][1], hand.thumb[2][0], hand.thumb[2][1])
        line(hand.thumb[2][0], hand.thumb[2][1], hand.thumb[3][0], hand.thumb[3][1])
        line(hand.indexFinger[0][0], hand.indexFinger[0][1], hand.indexFinger[1][0], hand.indexFinger[1][1])
        line(hand.indexFinger[1][0], hand.indexFinger[1][1], hand.indexFinger[2][0], hand.indexFinger[2][1])
        line(hand.indexFinger[2][0], hand.indexFinger[2][1], hand.indexFinger[3][0], hand.indexFinger[3][1])
        line(hand.middleFinger[0][0], hand.middleFinger[0][1], hand.middleFinger[1][0], hand.middleFinger[1][1])
        line(hand.middleFinger[1][0], hand.middleFinger[1][1], hand.middleFinger[2][0], hand.middleFinger[2][1])
        line(hand.middleFinger[2][0], hand.middleFinger[2][1], hand.middleFinger[3][0], hand.middleFinger[3][1])
        line(hand.ringFinger[0][0], hand.ringFinger[0][1], hand.ringFinger[1][0], hand.ringFinger[1][1])
        line(hand.ringFinger[1][0], hand.ringFinger[1][1], hand.ringFinger[2][0], hand.ringFinger[2][1])
        line(hand.ringFinger[2][0], hand.ringFinger[2][1], hand.ringFinger[3][0], hand.ringFinger[3][1])
        line(hand.pinky[0][0], hand.pinky[0][1], hand.pinky[1][0], hand.pinky[1][1])
        line(hand.pinky[1][0], hand.pinky[1][1], hand.pinky[2][0], hand.pinky[2][1])
        line(hand.pinky[2][0], hand.pinky[2][1], hand.pinky[3][0], hand.pinky[3][1])

        // palm lines
        line(hand.pinky[0][0], hand.pinky[0][1], hand.ringFinger[0][0], hand.ringFinger[0][1])
        line(hand.ringFinger[0][0], hand.ringFinger[0][1], hand.middleFinger[0][0], hand.middleFinger[0][1])
        line(hand.middleFinger[0][0], hand.middleFinger[0][1], hand.indexFinger[0][0], hand.indexFinger[0][1])
        line(hand.indexFinger[0][0], hand.indexFinger[0][1], hand.thumb[0][0], hand.thumb[0][1])
        line(hand.thumb[0][0], hand.thumb[0][1], hand.palmBase[0][0], hand.palmBase[0][1])
        line(hand.pinky[0][0], hand.pinky[0][1], hand.palmBase[0][0], hand.palmBase[0][1])

        // points
        for (j in handJob.landmarks){
            let keypoint = handJob.landmarks[j]
            fill(0,125,175)
            noStroke()
            ellipse(keypoint[0], keypoint[1], 5, 5)
        }
    }
}


function draw(){
    // background(255)
    image(video,0,0)
    drawPoints()
    // bounding(predictions)
}