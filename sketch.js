let w, h
let cols = 13
let rows = 13
let grid = new Array(cols)

let openSet = []
let closedSet = []
let start, end

function Cell(i,j){
    this.i = i
    this.j = j
    this.f = 0
    this.g = 0
    this.h = 0
    this.neighbors = []

    this.show = function(col){
        fill(col)
        noStroke()// stroke(0)
        rect(this.i * w, this.j * h, w-1, h-1)
    }

    this.addNeighbors = function(grid) {
        if (this.i < cols - 1){ 
            this.neighbors.push(grid[this.i+1][this.j]) 
        }
        if (this.i > 0){ this.neighbors.push(grid[this.i-1][this.j]) }
        if (this.j < rows - 1){ this.neighbors.push(grid[this.i][this.j+1]) }
        if (this.j > 0 ){ this.neighbors.push(grid[this.i][this.j-1]) }
    }
}

function removeFrom(arr, el) {
    for (let i = arr.length - 1; i >= 0; i--){
        if (arr[i] == el) {
            arr.splice(i, 1)
        }
    }
}

function setup(){
    createCanvas(400,400)
    w = width / cols
    h = height / rows
    console.log('A*')


    for (let i = 0; i < cols; i++ ){
        grid[i] = new Array(rows)
    }

    for (let i = 0; i < cols; i++ ){
        for (let j = 0; j < rows; j++ ){
            grid[i][j] = new Cell(i,j)
        }
    }

    for (let i = 0; i < cols; i++ ){
        for (let j = 0; j < rows; j++ ){
            grid[i][j].addNeighbors(grid)
        }
    }

    start = grid[0][0] //top left
    end = [cols - 1][rows - 1] //lower right

    openSet.push(start)

    console.log(grid)   
}

function draw(){

    if (openSet.length > 0) {
        let shortest = 0
        let current = openSet[shortest]

        for (let i = 0; i < openSet.length; i++){
            shortest = i
        }
        
        if (current == end) { console.log("DONE!")}

        closedSet.push(current)
        removeFrom(openSet, current)

        let neighbors = current.neighbors

        if (!closedSet.includes(neighbor)){
            let tempG = current.g + 1

            if (openSet.includes(neighbor)){
                if (tempG < n.g) {
                    n.g
                }
            }
        }
        for (let n of neighbors){
            // n.g = current + 1
        }


    } else {
        //no solution
    }

    background(0)

    for (let i = 0; i < cols; i++ ){
        for (let j = 0; j < rows; j++ ){
            grid[i][j].show(color(255))
        }
    }

    for (var i = 0; i < closedSet.length; i++){
        closedSet[i].show(color(255,0,0))
    }

    for (var i = 0; i < openSet.length; i++){
        openSet[i].show(color(0,255,0))
    }
}

