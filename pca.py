#!/usr/bin/env python

import json
import numpy as np
import pandas as pd
from sklearn import decomposition
import plotly.express as px

def principleComponent(subset):
    FEATURES = ["x", "y", "z"]
    pca = decomposition.PCA(n_components=1)
    pca_transformed = pca.fit_transform(subset[FEATURES])
    # print(indexPoint.shape)
    # print(pca_transformed.shape)
    # print(pca.components_)
    # print(pca.explained_variance_ratio_*100)
    pca_df = pd.DataFrame(pca_transformed)
    timeList = subset["time"].tolist()
    pca_df.insert(0, "time", timeList, True)
    # print(pca_df.head())
    return pca_df

def load():
    with open("handsDataTest.json", "r") as f:
        return json.load(f)

def graph(pca_df):
    fig = px.scatter_matrix(
        pca_df,
        dimensions=["time", 0],
        color=0,
        # labels={col for col in df.columns}
        # template="simple_white"
    )
    fig.update_traces(diagonal_visible=False)
    fig.show()

def main():
    data = load()
    df = pd.json_normalize(data)
    # print(df.head())

    # one hand only
    subset = df[df["hand"] == 1] 
    print(subset.head())
    # one finger point only
    # indexPoint = subset[subset["point"] == 8]
    # print(indexPoint.head())


    for hand_idx in range(2):
        for knuckle_idx in range(20):
            subset = df[df["hand"] == hand_idx]
            subset = subset[subset["point"] == knuckle_idx]
            pca_df = principleComponent(subset)
            graph(pca_df)

if __name__ == "__main__":
    main()